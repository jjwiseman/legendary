---
title: Masterlist
description: US Fixed Wing Asset Tracking Masterlist
menu: main
weight: -210
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum

## E - Special Electronic

### E-3 Sentry

Track the entire USAF fleet {{< adsbx electronic.sentry.usaf >}}via adsbx{{< /adsbx >}}

Track the entire NATO fleet {{< adsbx electronic.sentry.nato >}}via adsbx{{< /adsbx >}}

USAF:

{{< aircraft-table electronic.sentry.usaf >}}

NATO:

{{< aircraft-table electronic.sentry.nato >}}

## R - Recon

### RC-135S Cobra Ball

The greatest aircraft known to man, with the coolest mission possible! Chasing ballistic missile launches! Using MASINT with an array of optical cameras and SIGINT with an array of antenna's it can accurately gather range data and ensure treaty compliance. Based at Offutt Air Force Base as part of the 55th Operations Group in the 45th Reconnaissance Squadron with the wider O/RC/TC/WC-135 fleet where a total of 3 are operated. In the past, several were forward deployed to Shemya Air Force Base in Alaska but since the end of the Cold War this has ceased and today you generally find one forward deployed to Kadena Air Base in Okinawa Japan. 

[US Air Force info page](https://www.af.mil/About-Us/Fact-Sheets/Display/Article/104498/rc-135s-cobra-ball/)

{{< adsbx recon.cobraball >}}Track via adsbx{{< /adsbx >}}

#### Fleet

{{< aircraft-table recon.cobraball >}}

### RC-135U Combat Sent

The Combat Sent is the god of ELINT. With it's vast array of sensors, it locate, identify and analyse electronic emissions from air, land and sea forces. Based at Offutt Air Force Base as part of the 55th Operations Group in the 45th Reconnaissance Squadron with the wider O/RC/TC/WC-135 fleet where a total of 2 are operated. 

[US Air Force info page](https://www.af.mil/About-Us/Fact-Sheets/Display/Article/104495/rc-135u-combat-sent/)

{{< adsbx recon.combatsent >}}Track via adsbx{{< /adsbx >}}

#### Fleet

{{< aircraft-table recon.combatsent >}}

